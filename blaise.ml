let eval infile =
  let inchannel = if infile = "_" then stdin else open_in infile in
    Evaluation.eval inchannel

let pretty_print infile =
  let inchannel = if infile = "_" then stdin else open_in infile in
    Pretty.print inchannel

let specs =
  [
    ("-eval", Arg.String(eval), "execute the input");
  ]

let usage =
  Printf.sprintf
    "Usage: %s [-eval] file\n\
     \tIf file is '_' (underscore), the program will read on stdin"
    (Sys.argv.(0))

let _ =
    Arg.parse (Arg.align specs) (pretty_print) usage

