let write args =
  print_endline
    (String.concat " " (Array.to_list (Array.map string_of_int args)));
  9

let read _ = read_int ()

let get_stdlib () =
  let stdlib = Hashtbl.create 1 in
    Hashtbl.add stdlib "write" write;
    Hashtbl.add stdlib "read" read;
    stdlib
