###############
# {{{ Sources #
###############
MAIN := blaise.ml
SOURCE := blaise.ml pretty.ml evaluation.ml stdlib.ml
GIVEN := ast.mli lexer.ml parser.ml parser.mli syntax
TEST_SOURCE := test/test.sh test/test.in
TEST_FOLDER := test
TEST := test/test.sh
# }}}

#################
# {{{ Compiling #
#################
COMPILE := ocamlbuild
BUILDTYPE := native

all: blaise

.PHONY: blaise
blaise: $(SOURCE)
	@echo "Compiling…"
	@$(COMPILE) blaise.$(BUILDTYPE)
	@mv blaise.$(BUILDTYPE) blaise
	@echo "Done"
# }}}

##############
# {{{ Submit #
##############
LOGIN := hervot_p
GROUP := b2
PASS := :`cat rendu_pass`
URL := http://rendu.infoprepa.epita.fr/EPITA2016/index.php?TP=20130107

FOLDER := $(LOGIN)-mpoc2012
TARBALL := $(FOLDER).tar.bz2
TEMP_RENDU := log_rendu

POST := curl --silent\
	    -u $(LOGIN)$(PASS)\
	    -F GROUPE=$(GROUP)\
	    -F userfile=@$(TARBALL)\
	    -F UPLOADED=DONE\
	    -F MAX_FILE_SIZE=2097152\
	    $(URL)
CONFIRM := grep -e ">2012-12\|>2013-01" $(TEMP_RENDU)

.PHONY: rendu_prepare
rendu_prepare:
	@echo "Preparing Tarball…"
	@mkdir $(FOLDER)
	@cp -r $(SOURCE) $(GIVEN) $(TEST_FOLDER) Makefile $(FOLDER)
	@tar cf $(TARBALL) $(FOLDER)
	@rm -rf $(FOLDER)

.PHONY: rendu
rendu: rendu_prepare
	@echo "Sending…"
	@$(POST) > $(TEMP_RENDU)
	@echo "Last submit (juste make sure it's now):"
	@$(CONFIRM)
	@rm -rf $(TEMP_RENDU) $(TARBALL)
	@echo "Done"
# }}}

############
# {{{ Misc #
############
.PHONY: test
test: blaise $(TEST_SOURCE)
	./$(TEST)

.PHONY: syntax
syntax: $(SOURCE)
	python syntax.py $(SOURCE)

.PHONY: clean
clean:
	@echo "cleaning…"
	@$(COMPILE) -quiet -clean
	@rm -rf $(TARBALL) $(FOLDER) $(TEMP_RENDU)
# }}}
