#!/bin/sh

echo
echo "Testing pretty print:"
echo "-------------------------------------------------------------------------"
./blaise test/test.in > temp
cat temp
./blaise temp > temp2
echo "-------------------------------------------------------------------------"
echo
echo "Verify pretty print with diff: (empty is ok)"
echo "-------------------------------------------------------------------------"
diff temp temp2
echo "-------------------------------------------------------------------------"
rm temp temp2
echo
echo "Testing evaluation:"
echo "-------------------------------------------------------------------------"
./blaise -eval test/test.in
echo "-------------------------------------------------------------------------"
echo
