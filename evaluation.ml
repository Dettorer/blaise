open Ast

type func_env =
{
    mutable args:(string, int) Hashtbl.t;
    oargs:string array;
    mutable vars:(string, int) Hashtbl.t;
    mutable inst:statement array;
}

type env =
{
    mutable functions:(string, func_env ref) Hashtbl.t;
    stdlib:(string, (int array -> int)) Hashtbl.t;
}

let rec op_of_string = function
  | "+" -> (+)
  | "-" -> (-)
  | "*" -> ( * )
  | "/" -> (/)
  | ">" -> (fun a b -> if a > b then 1 else 0)
  | "<" -> (fun a b -> if a < b then 1 else 0)
  | ">=" -> (fun a b -> if a >= b then 1 else 0)
  | "<=" -> (fun a b -> if a <= b then 1 else 0)
  | "==" -> (fun a b -> if a == b then 1 else 0)
  | "||" -> (fun a b -> if a!=0 || b!=0 then 1 else 0)
  | "&&" -> (fun a b -> if a!=0 && b!=0 then 1 else 0)
  | op -> failwith (Pretty.fmt "Binary operator not recognized: '%s'" op)

let uniop_of_string = function
  | "!" -> (fun e -> if e = 0 then 1 else 0)
  | "-" -> (-) 0
  | op -> failwith (Pretty.fmt "Unary operator not recognized: '%s'" op)

let rec eval_expr func env = function
  | Int(i) ->
     i
  | Var(name) ->
     (try
         Hashtbl.find (!func.vars) name
     with Not_found ->
         Hashtbl.find (!func.args) name)
  | BinOp(e1, op, e2) ->
     (op_of_string op) (eval_expr func env e1) (eval_expr func env e2)
  | UniOp(op, e) ->
      (uniop_of_string op) (eval_expr func env e)
  | Call(name, args) ->
      if Hashtbl.mem (!env.functions) name then
        eval_func
          env
          (Hashtbl.find (!env.functions) name)
          (Array.map (eval_expr func env) (Array.of_list args))
      else
        try
          eval_stdlib_func
            env
            name
            (Array.map (eval_expr func env) (Array.of_list args))
        with Not_found ->
          failwith (Pretty.fmt "Function not found: '%s'" name)


and eval_statement env func return = function
  | Assign(name, value) ->
      Hashtbl.replace (!func.vars) name (eval_expr func env value)
  | Expr(value) ->
      ignore (eval_expr func env value)
  | If(cond, if_inst, else_inst) ->
      if (eval_expr func env cond) != 0
      then List.iter (eval_statement env func return) if_inst
      else List.iter (eval_statement env func return) else_inst
  | While(cond, inst) ->
      while (eval_expr func env cond) != 0 do
        List.iter (eval_statement env func return) inst
      done
  | Return(expr) ->
      return := (true, eval_expr func env expr)

and eval_func env func args =
  Array.iteri
    (fun i e -> Hashtbl.replace (!func.args) (!func.oargs.(i)) e)
    args;
  let return = ref (false, 0) in
  let i = ref 0 in
  while !i < Array.length (!func.inst) && !return = (false, 0) do
    eval_statement env func return (!func.inst.(!i));
    i := !i + 1
  done;
  let (_,return_value) = !return in
    return_value

and eval_stdlib_func env name args =
  (Hashtbl.find (!env.stdlib) name) args

let process env =
  ignore (eval_func env (Hashtbl.find (!env.functions) "main") [||])

let build_function env {fname=fname;fbody=fbody;fparams=fparams;fvars=fvars} =
  let nargs, nvars = (List.length fparams, List.length fvars) in
  let func =
    {
      args = Hashtbl.create (nargs);
      oargs = Array.of_list fparams;
      vars = Hashtbl.create (nvars);
      inst = Array.of_list fbody;
    }
  in
    List.iter (fun name -> Hashtbl.add (func.vars) name 0) fvars;
    List.iter (fun name -> Hashtbl.add (func.args) name 0) fparams;
    Hashtbl.add (!env.functions) fname (ref func)

let build_main env {mainvars=mainvars;mainbody=mainbody} =
  let nvars = List.length mainvars in
  let main =
    {
      args = Hashtbl.create 0;
      oargs= [||];
      vars = Hashtbl.create (nvars);
      inst = Array.of_list mainbody;
    }
  in
    List.iter (fun name -> Hashtbl.add (main.vars) name 0) mainvars;
    Hashtbl.add (!env.functions) "main" (ref main)

let build_env ast =
  let env = ref
    {
      functions = Hashtbl.create (List.length ast.func + 1);
      stdlib = Stdlib.get_stdlib ()
    }
  in
    build_main env (ast.main);
    List.iter (build_function env) (ast.func);
    env

let eval inchannel =
  let ast = Parser.prg Lexer.token (Lexing.from_channel inchannel) in
    process (build_env ast)
