open Ast

let fmt = Printf.sprintf

let softtabstop = 4
let expandtab = true

let indent i =
  if expandtab then String.make (softtabstop * i) ' '
  else String.make i '\t'

let rec deparse_expr parenthesis = function
  | Int(i) -> string_of_int i
  | Var(v) -> v
  | BinOp(e1, op, e2) ->
      if parenthesis then
        fmt "(%s %s %s)" (deparse_expr true e1) op (deparse_expr true e2)
      else
        fmt "%s %s %s" (deparse_expr true e1) op (deparse_expr true e2)
  | UniOp(op, e) ->
      fmt "%s%s" op (deparse_expr true e)
  | Call(name, args) ->
      fmt
        "%s(%s)"
        name
        (String.concat ", " (List.map (deparse_expr false) args))

let rec deparse_statements buf i = function
  | [] -> ()
  | e::l -> statement buf i e; deparse_statements buf i l

and statement buf i = function
  | Assign(var, expr) -> Buffer.add_string (!buf)
        (fmt "%s%s = %s;\n" (indent i) var (deparse_expr false expr))
  | Expr(expr) -> Buffer.add_string (!buf)
        (fmt "%s%s;\n" (indent i) (deparse_expr false expr))
  | If(cond, ifinst, elseinst) ->
      begin
        Buffer.add_string
          (!buf)
          (fmt "\n%sif(%s)\n" (indent i) (deparse_expr false cond));
        Buffer.add_string (!buf) (fmt "%s{\n" (indent i));
        deparse_statements buf (i+1) ifinst;
        Buffer.add_string (!buf) (fmt "%s}\n" (indent i));
        if elseinst = [] then () else
          begin
            Buffer.add_string (!buf) (fmt "%selse\n" (indent i));
            Buffer.add_string (!buf) (fmt "%s{\n" (indent i));
            deparse_statements buf (i+1) elseinst;
            Buffer.add_string (!buf) (fmt "%s}\n" (indent i))
          end;
        Buffer.add_string (!buf) "\n"
      end

  | While(cond, inst) ->
      begin
        Buffer.add_string
          (!buf)
          (fmt "\n%swhile(%s)\n" (indent i) (deparse_expr false cond));
        Buffer.add_string (!buf) (fmt "%s{\n" (indent i));
        deparse_statements buf (i+1) inst;
        Buffer.add_string (!buf) (fmt "%s}\n" (indent i));
      end

  | Return(expr) -> Buffer.add_string (!buf)
        (fmt "%sreturn(%s);\n" (indent i) (deparse_expr false expr))

let deparse_function buf i func =
  begin
    Buffer.add_string (!buf) (fmt "%s%s(%s)\n{\n"
      (indent i) (func.fname) (String.concat ", " func.fparams));
    if func.fvars = [] then () else
      Buffer.add_string (!buf)
        (fmt "%svars %s;\n\n" (indent 1) (String.concat "," func.fvars));
    deparse_statements buf 1 (func.fbody);
    Buffer.add_string (!buf) "}\n\n"
  end

let deparse ast =
  let buf = ref (Buffer.create 80) in
    begin
      List.iter (deparse_function buf 0) (ast.func);
      deparse_function buf 0
        ({fname="main";
          fbody=ast.main.mainbody;
          fparams=[];
          fvars=ast.main.mainvars
        });
      Buffer.contents (!buf)
    end

let print inchannel =
  let ast = Parser.prg Lexer.token (Lexing.from_channel inchannel) in
    print_endline (deparse ast)
